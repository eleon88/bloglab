export class BlogCommentCreate {
  constructor(
    public blogCommentId: number,
    public BlogId: number,
    public content: string,
    public parentBlogCommentId?: number
  ){}
}
